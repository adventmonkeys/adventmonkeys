# Advent monkeys

## Todo:

- [X] [Read config with viper](https://github.com/paluszkiewiczB/timeToGo/tree/master/viperCfg)
- [X] Use Go templates to make configurable WorryLevelOperation
- [ ] Improve monkey's items in injectable storage
      - [ ] Allow for injectable storage
      - [ ] Integrate with popular external storage backends:
  - [ ] RDBMS
  - [ ] Redis
  - [ ] File system
  - [ ] MongoDB
  - [ ] S3/Minio
- [ ] Improve monitoring:
      - [ ] Metrics
      - [ ] **Structured** logs (logrus or experimental logs?)
      - [ ] Traces
- [ ] Write ADRs
- [ ] Write GitLab CI pipelines:
      - [ ] On every push:
  - [ ] Unit tests
  - [ ] Integration tests
  - [ ] Static checks: formatting, linting
      - [ ] Release - when previous one passes and code can be tagged:
  - [ ] Tag version
  - [ ] Build Docker image
  - [ ] Build Helm chart
- [ ] Try fuzzing
- [ ] Try Air when it will make sense