package main

import "testing"

func TestOk(t *testing.T) {
}

func TestTmpl(t *testing.T) {
	cfg := readConfig()
	_, err := BuildWorryLvlTest(*cfg.Monkey.WorryLevelTest)
	if err != nil {
		t.Fatal(err)
	}
	_, err = BuildWorryLvlOp(*cfg.Monkey.WorryLevelOp)
	if err != nil {
		t.Fatal(err)
	}
}
