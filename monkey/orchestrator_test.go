package main

import (
	"context"
	"fmt"
	"log"
	"testing"
	"time"
)

func TestRetryingOrchestrator_Orchestrate(t *testing.T) {
	t.Run("should return nil immediately when next returns nil", func(t *testing.T) {
		next := &okAfter{times: 0}
		ro := retryingOrchestrator{
			Delay:  time.Nanosecond,
			Tries:  5,
			Logger: &log.Logger{},
			next:   next,
		}
		err := ro.Orchestrate(context.Background())
		if err != nil {
			t.Errorf("unexpected error: %s", err)
		}
		if next.tries != 1 {
			t.Errorf("expected 1 tries, actual: %d", next.tries)
		}
	})

	t.Run("should return after n-th time when number of tries did not exceed the defined max tries", func(t *testing.T) {
		tt := map[string]struct {
			okAfterTimes int
			maxTimes     int
		}{
			"1 out of 2":  {1, 1},
			"1 out of 1":  {1, 1},
			"5 out of 10": {1, 1},
		}

		for name, tc := range tt {
			t.Run(name, func(t *testing.T) {
				next := &okAfter{times: tc.okAfterTimes}
				ro := retryingOrchestrator{
					Delay:  time.Nanosecond,
					Tries:  tc.maxTimes,
					Logger: &log.Logger{},
					next:   next,
				}

				err := ro.Orchestrate(context.Background())
				if err != nil {
					t.Errorf("unexpected error: %s", err)
				}

				if next.tries != tc.okAfterTimes {
					t.Errorf("expected %d tries, actual: %d", tc.okAfterTimes, next.times)
				}
			})
		}
	})

	// TODO tests for returning non-nil error
}

type okAfter struct {
	tries int
	times int
}

func (a *okAfter) Orchestrate(_ context.Context) error {
	a.tries++
	if a.tries >= a.times {
		return nil
	}

	return fmt.Errorf("dummy error")
}
