package main

import (
	"context"
	"fmt"
	"log"
	"strconv"
)

type (
	WorryLevel          int
	Item                int
	Items               []Item
	WorryLevelOperation func(l WorryLevel) WorryLevel
	WorryLevelTest      func(l WorryLevel) bool
)

// Ider identifies the resource
type Ider interface {
	Id() string
}

type stringIdentifier struct {
	id string
}

func (s *stringIdentifier) Id() string {
	return s.id
}

func IntId(i int) Ider {
	return StringId(strconv.Itoa(i))
}

func StringId(s string) Ider {
	return &stringIdentifier{id: s}
}

type Inspector interface {
	Ider
	Inspect(ctx context.Context) error
}

type Catcher interface {
	Catch(ctx context.Context, what Item) error
}

type Thrower interface {
	Throw(ctx context.Context, what Item, to Ider) error
}

type Monkey struct {
	Ider
	thrower Thrower
	*log.Logger
	items          Items
	operation      WorryLevelOperation
	boredTest      WorryLevelTest
	nextMonkeyTest WorryLevelTest
	onTestTrue     Ider
	onTestFalse    Ider
}

func (m *Monkey) Id() string {
	return fmt.Sprintf("monkey %s", m.Ider.Id())
}

func (m *Monkey) Inspect(ctx context.Context) error {
	for _, wl := range m.items {
		select {
		case <-ctx.Done():
			return fmt.Errorf("monkey %s inspect: %w", m.Id(), ctx.Err())
		default:
			level := m.operation(WorryLevel(wl))
			if m.boredTest(level) {
				level %= 3
			}
			if m.nextMonkeyTest(level) {
				err := m.thrower.Throw(ctx, Item(level), m.onTestTrue)
				if err != nil {
					return err
				}
			} else {
				err := m.thrower.Throw(ctx, Item(level), m.onTestFalse)
				if err != nil {
					return err
				}
			}
		}
	}
	return nil
}

func (m *Monkey) Catch(_ context.Context, what Item) error {
	m.Logger.Printf("monkey %s catching item: %d", m.Id(), what)
	m.items = append(m.items, what)
	return nil
}

func NewMonkey(id Ider, opts ...CreateOption) *Monkey {
	m := &Monkey{
		Ider:  id,
		items: make([]Item, 0),
		operation: func(l WorryLevel) WorryLevel {
			return l
		},
		boredTest: func(l WorryLevel) bool {
			return true
		},
		nextMonkeyTest: func(l WorryLevel) bool {
			return true
		},
		onTestTrue:  StringId(""),
		onTestFalse: StringId(""),
	}

	for _, opt := range opts {
		opt(m)
	}

	return m
}

type CreateOption func(m *Monkey)

func WithItems(items []int) CreateOption {
	out := make([]Item, len(items))
	for i, item := range items {
		out[i] = Item(item)
	}

	return func(m *Monkey) {
		m.items = out
	}
}

func WithLogger(l *log.Logger) CreateOption {
	return func(m *Monkey) {
		m.Logger = l
	}
}

func WithOperation(op WorryLevelOperation) CreateOption {
	return func(m *Monkey) {
		m.operation = op
	}
}

func WithNextMonkeyTest(t WorryLevelTest) CreateOption {
	return func(m *Monkey) {
		m.nextMonkeyTest = t
	}
}

func WithBoredTest(t WorryLevelTest) CreateOption {
	return func(m *Monkey) {
		m.boredTest = t
	}
}

func WhenTestPassesSendTo(id Ider) CreateOption {
	return func(m *Monkey) {
		m.onTestTrue = id
	}
}

func WhenTestFailsSendTo(id Ider) CreateOption {
	return func(m *Monkey) {
		m.onTestFalse = id
	}
}

func WithThrower(t Thrower) CreateOption {
	return func(m *Monkey) {
		m.thrower = t
	}
}
