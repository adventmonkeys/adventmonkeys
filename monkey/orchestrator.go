package main

import (
	"context"
	"fmt"
	"log"
	"time"
)

type orchestrator interface {
	Orchestrate(ctx context.Context) error
}

type retryingOrchestrator struct {
	Delay time.Duration
	Tries int
	*log.Logger
	next orchestrator
}

func (o *retryingOrchestrator) Orchestrate(ctx context.Context) error {
	err := o.next.Orchestrate(ctx)
	for i := 0; i < o.Tries; i++ {
		if err == nil {
			return nil
		}
		o.Logger.Printf("orchestrating failed, error: %s\n", err)
		o.Logger.Printf("retry number %d out of %d\n", i, o.Tries)
		timer := time.NewTimer(o.Delay)
		<-timer.C
		err = o.next.Orchestrate(ctx)
	}

	return fmt.Errorf("max retries exceeded: %w", err)
}

type iteratingOrchestrator struct {
	iterations int
	next       orchestrator
	*log.Logger
}

func (o *iteratingOrchestrator) Orchestrate(ctx context.Context) error {
	for i := 0; i < o.iterations; i++ {
		o.Logger.Printf("start orchestrating iteration: %d", i)
		err := o.next.Orchestrate(ctx)
		if err != nil {
			return fmt.Errorf("orchestrate iteration %d: %s", i, err)
		}
	}

	return nil
}

type inspectingOrchestrator struct {
	inspectors []Inspector
}

func (o *inspectingOrchestrator) Orchestrate(ctx context.Context) error {
	for i, ins := range o.inspectors {
		err := ins.Inspect(ctx)
		if err != nil {
			return fmt.Errorf("inspector number %d (id: %s): %s", i, ins.Id(), err)
		}
	}
	return nil
}
