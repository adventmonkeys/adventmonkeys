package main

import (
	"context"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

func newTracer() trace.Tracer {
	return otel.Tracer("advent-monkeys")
}

type tracingInspector struct {
	tracer trace.Tracer
	next   Inspector
}

func (i *tracingInspector) Inspect(ctx context.Context) (err error) {
	tracedCtx, span := i.tracer.Start(ctx, "inspect")
	defer func() {
		if err != nil {
			span.AddEvent("error", trace.WithAttributes(attribute.String("err", err.Error())))
		}
		span.End()
	}()
	span.SetAttributes(attribute.String("inspector id", i.next.Id()))
	return i.next.Inspect(tracedCtx)
}

func (i *tracingInspector) Id() string {
	return i.next.Id()
}

type tracedThrower struct {
	tracer trace.Tracer
	next   Thrower
}

func (t *tracedThrower) Throw(ctx context.Context, what Item, to Ider) (err error) {
	tracedCtx, span := t.tracer.Start(ctx, "throw")
	defer func() {
		if err != nil {
			span.AddEvent("error", trace.WithAttributes(attribute.String("err", err.Error())))
		}
		span.End()
	}()
	span.SetAttributes(
		attribute.Int("item", int(what)),
		attribute.String("to", to.Id()),
	)
	return t.next.Throw(tracedCtx, what, to)
}

type tracedCatcher struct {
	tracer trace.Tracer
	next   Catcher
}

func (t *tracedCatcher) Catch(ctx context.Context, what Item) (err error) {
	spanCtx, span := t.tracer.Start(ctx, "catch")
	defer func() {
		if err != nil {
			span.AddEvent("error", trace.WithAttributes(attribute.String("err", err.Error())))
		}
		span.End()
	}()

	return t.next.Catch(spanCtx, what)
}
