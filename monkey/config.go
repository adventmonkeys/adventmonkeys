package main

import (
	"flag"
	"log"
	"os"

	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

type rootCfg struct {
	Monkey *monkeyCfg
}

func defaultCfg() *rootCfg {
	return &rootCfg{
		defaultMonkeyCfg(),
	}
}

type monkeyCfg struct {
	Id             string
	WorryLevelOp   *worryLvlOpCfg
	WorryLevelTest *worryLvlTestCfg
	Throw          *throwCfg
}

func defaultMonkeyCfg() *monkeyCfg {
	return &monkeyCfg{
		WorryLevelOp:   defaultWorryLvlOpCfg(),
		WorryLevelTest: defaultWorryLvlTestCfg(),
		Throw:          defaultThrowCfg(),
	}
}

type worryLvlOpMethod string

const (
	// does not change worry level
	noopOp worryLvlOpMethod = "noop"
	// uses text/template to calculate worry level
	templateOp = "template"
)

type worryLvlOpCfg struct {
	Method   worryLvlOpMethod
	Template *tplMethCfg
}

func defaultWorryLvlOpCfg() *worryLvlOpCfg {
	return &worryLvlOpCfg{
		Method: noopOp,
	}
}

type tplMethCfg struct {
	Template string
}

type worryLvlTestMethod string

const (
	// makes every test pass
	okTest worryLvlTestMethod = "ok"
	// uses text/template to calculate worry level
	templateTest = "template"
)

type worryLvlTestCfg struct {
	Method   worryLvlTestMethod
	Template *tplMethCfg
}

func defaultWorryLvlTestCfg() *worryLvlTestCfg {
	return &worryLvlTestCfg{
		Method: okTest,
	}
}

type throwCfg struct {
	TestOk, TestNok string
}

func defaultThrowCfg() *throwCfg {
	return &throwCfg{}
}

func readConfig() *rootCfg {
	v := newViper("config.yaml")
	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	pflag.Parse()
	must(v.ReadInConfig())

	c := defaultCfg()
	log.Printf("default config struct: %+v\n", c)

	must(v.Unmarshal(c))
	log.Printf("after reading config.yaml: %+v\n", c)

	if _, err := os.Stat("override.yaml"); err != nil {
		// nothing to override
		return c
	}

	override := newViper("override.yaml")
	must(override.ReadInConfig())
	must(override.Unmarshal(c))
	log.Printf("after reading override.yaml: %+v\n", c)

	return c
}

func newViper(cfgFile string) *viper.Viper {
	v := viper.New()
	v.SetConfigFile(cfgFile)
	v.SetConfigType("yaml")
	v.AutomaticEnv()
	return v
}

func must(err error) {
	if err != nil {
		panic(err)
	}
}
