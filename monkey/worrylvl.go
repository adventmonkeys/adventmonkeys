package main

import (
	"bytes"
	"fmt"
	"strconv"
	gotpl "text/template"

	"github.com/Masterminds/sprig/v3"
)

func BuildWorryLvlTest(cfg worryLvlTestCfg) (WorryLevelTest, error) {
	switch cfg.Method {
	case okTest:
		return noopTestFunc, nil
	case templateTest:
		return templateTestFunc(*cfg.Template)
	}
	return nil, fmt.Errorf("unsupported worry level operation method: %s", cfg.Method)
}

func BuildWorryLvlOp(cfg worryLvlOpCfg) (WorryLevelOperation, error) {
	switch cfg.Method {
	case noopOp:
		return noopOpFunc, nil
	case templateOp:
		return templateOpFunc(*cfg.Template)
	}
	return nil, fmt.Errorf("unsupported worry level operation method: %s", cfg.Method)
}

func noopTestFunc(_ WorryLevel) bool {
	return true
}

func templateTestFunc(cfg tplMethCfg) (WorryLevelTest, error) {
	parsed, err := parseTpl(cfg)
	if err != nil {
		return nil, fmt.Errorf("parsing worry level test: %w", err)
	}

	err = testBoolReturningTemplate(parsed)
	if err != nil {
		return nil, fmt.Errorf("testing template test func: %w", err)
	}

	return func(l WorryLevel) bool {
		out, err := execWithLevel(l, parsed)
		if err != nil {
			panic(fmt.Errorf("executing worry level test: %w", err))
		}
		result, err := strconv.ParseBool(out)
		if err != nil {
			panic(fmt.Errorf("converting worry level test reult to bool: %w", err))
		}
		return result
	}, nil
}

func noopOpFunc(i WorryLevel) WorryLevel {
	return i
}

func templateOpFunc(cfg tplMethCfg) (WorryLevelOperation, error) {
	parsed, err := parseTpl(cfg)
	if err != nil {
		return nil, fmt.Errorf("parsing worry level operation: %w", err)
	}

	err = testIntReturningTemplate(parsed)
	if err != nil {
		return nil, fmt.Errorf("testing template op func: %w", err)
	}

	return func(l WorryLevel) WorryLevel {
		out, err := execWithLevel(l, parsed)
		if err != nil {
			panic(fmt.Errorf("execuring worry level operation: %w", err))
		}
		lvl, err := strconv.Atoi(out)
		if err != nil {
			panic(fmt.Errorf("converting worry level template result to int: %w", err))
		}
		return WorryLevel(lvl)
	}, nil
}

func execWithLevel(l WorryLevel, parsed *gotpl.Template) (string, error) {
	out := &bytes.Buffer{}
	err := parsed.Execute(out, levelData(l))
	if err != nil {
		return "", err
	}
	return out.String(), err
}

func parseTpl(cfg tplMethCfg) (*gotpl.Template, error) {
	t := gotpl.New("worrylvl").Funcs(sprig.FuncMap())
	parsed, err := t.Parse(cfg.Template)
	if err != nil {
		return nil, fmt.Errorf("parse worry level template: %w", err)
	}
	return parsed, nil
}

func levelData(l WorryLevel) map[string]interface{} {
	return map[string]interface{}{"level": int(l)}
}

func testIntReturningTemplate(t *gotpl.Template) error {
	out, err := execWithLevel(WorryLevel(1), t)
	if err != nil {
		return fmt.Errorf("text exec of int returning template: %w", err)
	}

	_, err = strconv.Atoi(out)
	if err != nil {
		return fmt.Errorf("converting result of test exec to int: %w", err)
	}

	return nil
}

func testBoolReturningTemplate(parsed *gotpl.Template) error {
	out, err := execWithLevel(WorryLevel(1), parsed)
	if err != nil {
		return fmt.Errorf("test exec of bool returning template: %w", err)
	}

	_, err = strconv.ParseBool(out)
	if err != nil {
		return fmt.Errorf("converting result of test exec to bool: %w", err)
	}

	return nil
}
