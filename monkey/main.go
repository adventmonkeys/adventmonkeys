package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"go.opentelemetry.io/otel/trace"
)

func main() {
	ctx := context.Background()
	cfg := readConfig()
	log.Printf("config: %+v", cfg)

	logger := log.New(os.Stdout, "", log.Ldate|log.Ltime|log.LUTC)
	tracer := newTracer()

	thrower := NewThrower(logger, tracer, WithUpstream(":8081", IntId(1)))

	monkey, err := buildMonkey(thrower, logger, cfg.Monkey)
	if err != nil {
		logger.Fatalf("building monkey: %s", err)
	}

	server := buildMonkeyServer(tracer, logger, monkey)
	go server.ListenAndServe(fmt.Sprintf(":%d", 8080))

	time.Sleep(time.Second * 1)

	monkeys := instrumentedMonkeys(logger, tracer, monkey)
	o := iteratingOrchestrator{
		iterations: 3,
		Logger:     logger,
		next: &retryingOrchestrator{
			Delay:  time.Millisecond * 10,
			Tries:  5,
			Logger: logger,
			next: &inspectingOrchestrator{
				inspectors: monkeys,
			},
		},
	}

	err = o.Orchestrate(ctx)
	if err != nil {
		logger.Fatalf("start orchestrator %s", err)
	}
}

func buildMonkey(thrower Thrower, logger *log.Logger, monkey *monkeyCfg) (*Monkey, error) {
	op, err := BuildWorryLvlOp(*monkey.WorryLevelOp)
	if err != nil {
		return nil, err
	}

	test, err := BuildWorryLvlTest(*monkey.WorryLevelTest)
	if err != nil {
		return nil, err
	}

	return NewMonkey(StringId(monkey.Id),
		WithItems([]int{1}),
		WhenTestFailsSendTo(StringId(monkey.Throw.TestOk)),
		WhenTestPassesSendTo(StringId(monkey.Throw.TestOk)),
		WithThrower(thrower),
		WithLogger(logger),
		WithOperation(op),
		WithBoredTest(test),
		// WithNextMonkeyTest()
	), nil
}

func instrumentedMonkeys(l *log.Logger, t trace.Tracer, monkeys ...*Monkey) []Inspector {
	out := make([]Inspector, len(monkeys))
	for i, m := range monkeys {
		out[i] = &loggingInspector{
			Inspector: &tracingInspector{
				next:   m,
				tracer: t,
			},
			Logger: l,
		}
	}

	return out
}

type loggingInspector struct {
	Inspector
	*log.Logger
}

func (m *loggingInspector) Inspect(ctx context.Context) error {
	m.Logger.Printf("inspection by inspector: %s", m.Id())
	defer m.Logger.Printf("inspected by inspector: %s", m.Id())
	return m.Inspector.Inspect(ctx)
}

func buildMonkeyServer(tracer trace.Tracer, logger *log.Logger, catcher Catcher) MonkeyServer {
	return &loggingServer{
		Logger: logger,
		next: &catcherGrpcServer{
			Logger: logger,
			catcher: &tracedCatcher{
				tracer: tracer,
				next:   catcher,
			},
		},
	}
}

func NewThrower(l *log.Logger, t trace.Tracer, opts ...ThrowerOption) Thrower {
	thOpt := &throwerOpts{
		upstreams: []*upstreamOpt{},
	}

	for _, opt := range opts {
		opt(thOpt)
	}

	out := make(map[string]Thrower)
	for id, u := range thOpt.upstreams {
		out[u.id.Id()] = &tracedThrower{
			tracer: t,
			next:   newGrpcThrower(IntId(id), u.addr, l),
		}
	}

	return &loggingThrower{
		Logger: l,
		Thrower: &selectingThrower{
			upstreams: out,
		},
	}
}
