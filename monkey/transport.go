package main

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"net"

	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	pb "interactor.dev/advent-monkeys/monkey/proto"
)

type MonkeyServer interface {
	ListenAndServe(addr string)
	Stop()
}

type loggingServer struct {
	*log.Logger
	next MonkeyServer
}

func (l *loggingServer) ListenAndServe(addr string) {
	l.Logger.Printf("server will listen on addr: %s\n", addr)
	l.next.ListenAndServe(addr)
}

func (l *loggingServer) Stop() {
	l.Logger.Println("stopping server..")
	defer l.Logger.Println("server stopped..")
	l.next.Stop()
}

type catcherGrpcServer struct {
	catcher Catcher
	*log.Logger
	grpSrv *grpc.Server
	pb.UnimplementedMonkeyServer
}

func (s *catcherGrpcServer) Stop() {
	s.grpSrv.GracefulStop()
}

func (s *catcherGrpcServer) ListenAndServe(addr string) {
	lis, err := net.Listen("tcp", addr)
	if err != nil {
		s.Logger.Fatalf("failed to listen: %v", err)
	}
	s.grpSrv = grpc.NewServer(
		grpc.UnaryInterceptor(otelgrpc.UnaryServerInterceptor()),
		grpc.StreamInterceptor(otelgrpc.StreamServerInterceptor()),
	)
	pb.RegisterMonkeyServer(s.grpSrv, s)
	err = s.grpSrv.Serve(lis)
	if err != nil {
		panic(err)
	}
}

func (s *catcherGrpcServer) ThrowItem(ctx context.Context, request *pb.ThrowItemRequest) (*pb.ThrowItemResponse, error) {
	err := s.catcher.Catch(ctx, Item(request.ItemValue))
	if err != nil {
		return &pb.ThrowItemResponse{
			Result: pb.ThrowResult_RESULT_CLIENT_ERROR,
			Error:  fmt.Sprintf("catch item: %s", err),
		}, nil
	}

	return &pb.ThrowItemResponse{
		Result: pb.ThrowResult_RESULT_OK,
	}, nil
}

type ThrowerOption func(opt *throwerOpts)

func WithUpstream(addr string, id Ider) ThrowerOption {
	return func(opt *throwerOpts) {
		up := &upstreamOpt{
			addr: addr,
			id:   id,
		}
		opt.upstreams = append(opt.upstreams, up)
	}
}

type throwerOpts struct {
	upstreams []*upstreamOpt
}

type upstreamOpt struct {
	addr string
	id   Ider
}

type selectingThrower struct {
	upstreams map[string]Thrower
}

func (s *selectingThrower) Throw(ctx context.Context, what Item, to Ider) error {
	up, ok := s.upstreams[to.Id()]
	if !ok {
		return fmt.Errorf("unknown upstream for id: %s", to.Id())
	}
	return up.Throw(ctx, what, to)
}

type loggingThrower struct {
	*log.Logger
	Thrower
}

func (l *loggingThrower) Throw(ctx context.Context, what Item, to Ider) (err error) {
	l.Logger.Printf("throwing item: %d to: %d", what, to)
	defer func() {
		if err != nil {
			l.Logger.Printf("failed to throw item: %d to %s", what, to.Id())
		} else {
			l.Logger.Printf("thrown item: %d to: %s", what, to.Id())
		}
	}()
	return l.Thrower.Throw(ctx, what, to)
}

type grpcThrower struct {
	Ider
	pb.MonkeyClient
	*log.Logger
}

func newGrpcThrower(id Ider, addr string, l *log.Logger) *grpcThrower {
	clientInterface, err := grpc.Dial(addr,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithUnaryInterceptor(otelgrpc.UnaryClientInterceptor()),
		grpc.WithStreamInterceptor(otelgrpc.StreamClientInterceptor()),
	)
	if err != nil {
		panic(err)
	}
	client := pb.NewMonkeyClient(clientInterface)
	return &grpcThrower{
		Ider:         id,
		MonkeyClient: client,
		Logger:       l,
	}
}

func (t *grpcThrower) Throw(ctx context.Context, what Item, to Ider) error {
	response, err := t.ThrowItem(ctx,
		&pb.ThrowItemRequest{
			ThrowId:   rand.Uint64(),
			ItemValue: uint64(what),
		})
	if err != nil {
		return fmt.Errorf("throw from %s to :%d, %v", t, to, err)
	}
	log.Printf("result: %s", response.Result)

	switch response.Result {
	case pb.ThrowResult_RESULT_CLIENT_ERROR:
		log.Printf("error: %s\n", response.Error)
	case pb.ThrowResult_RESULT_SERVER_ERROR:
		log.Printf("error: %s\n", response.Error)
	}

	return nil
}

func (t *grpcThrower) String() string {
	return fmt.Sprintf("grpc thrower %s", t.Ider)
}
